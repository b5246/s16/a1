let userInput = Number(prompt("Enter a number: "));
console.log("The number that you provided is " + userInput +" .");

let currentValue = userInput;
while(currentValue > 0){
	if(currentValue <= 50){
		console.log("The current value is at "+currentValue+". Terminating the loop." )
		break;
	}
	if(currentValue % 5 == 0 && currentValue % 10 != 0 ){
		console.log(currentValue);
	} 
	if(currentValue % 10 == 0){
		console.log("The number is Divisible by 10. Skipping the number.");
	}
	currentValue--;
}

let userString = "supercalifragilisticexpialidocious";
console.log(userString);
let consonantString ="";


for(let i=0; i < userString.length; i++){
	if(userString[i].toLowerCase() === 'a' ||
		userString[i].toLowerCase() === 'e' ||
		userString[i].toLowerCase() === 'i' ||
		userString[i].toLowerCase() === 'o' ||
		userString[i].toLowerCase() === 'u' 
		){
		continue;
	}else{
		consonantString += userString[i];
	}
}
console.log(consonantString);
